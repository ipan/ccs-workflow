## CO2 storage workflow with OPM simulator


### Background links

-  https://opm-project.org/
- CO2 storage tutorials
  - https://opm-project.org/?p=1423
  - https://opm-project.org/wp-content/uploads/2019/02/sandve_opm_meeting_jan_2019.pdf
- Outputs from simulator can be visualised in open source visualiser ResInsight
  - https://resinsight.org/getting-started/
- Can be run in a docker container
  - https://opm-project.org/?page_id=853&page=5
- Sensitivity analysis using OPM and python
  - https://www.youtube.com/watch?v=t84yMCuHYT0
  - https://www.youtube.com/watch?v=qn0E7NyE1Ac
- DOE, ML using OPM
  - https://www.youtube.com/watch?v=4n8gtkZfxbY

### Installation on local machine

- Installation instructions on Ubuntu is [here](https://opm-project.org/?page_id=36) which works at one go.
- Check if the software is installed with 
    ``` flow --version
        flow --help
    ```

### Run a test case

- Automated test cases are [here](https://github.com/OPM/opm-tests)
- Run a basic cube model as shown [here](https://opm-project.org/?page_id=197)
  - Download the input file for SPE1CASE1 
  ``` wget https://raw.githubusercontent.com/OPM/opm-tests/master/spe1/SPE1CASE1.DATA```
  - Run case with
  ```flow SPE1CASE1.DATA --output-dir=cube-test-case```
  - The summary tool is called `summary.x` instead of `ecl_summary`
  - Get a list of different variables that are available to plot and manually inspect values
    ``` cd cube-test-case
        summary.x --list SPE1CASE1.DATA  
        summary.x SPE1CASE1.DATA WBHP:INJ
    ```
  - Plot the results using instructions [here](https://opm-project.org/?page_id=197&page=6)
    - `summaryplot` is a binary which can be used as follows
    ``` python summaryplot WBHP:INJ WBHP:PROD WOPR:PROD WGPR:PROD WGIR:INJ SPE1CASE1.DATA```

### Run a CO2 storage example
- We want to run one CO2 storage use case with input file [here](https://github.com/OPM/opm-tests/blob/master/co2store/CO2STORE.DATA)
- Run the test case with 
    ``` 
    flow CO2STORE.DATA --output-dir=CO2-test-case
    ```
- Check variables available to plot and inspect values
    ``` cd CO2-test-case/
        summary.x --list CO2STORE
        summary.x CO2STORE BPR:190
    ```
- Plot curves
    ``` python ../summaryplot BPR:190 BGSAT:10 BGSAT:190 CO2STORE.DATA```
- Post-processing visualisation
  - Download and install the open source visualisation tool ResInsight from [here](https://resinsight.org/getting-started/download-and-install/). 
  - Start the application using `ResInsight`.
  - Load result file and click play to see animation of CO2 injection


### Run real world CO2 test case

- Real world case studies are [here](https://co2datashare.org/)
- Trying out the Smeaheia dataset [here](https://co2datashare.org/dataset/smeaheia-dataset)
  - Download the simulation models zip file from here
  - TODO: The `CO2STORE` keyword has to be activated, else it solves for black oil
- Run the case with parallel processing (takes ~24 mins on my laptop with 4 MPI processes)
    ``` 
    mpirun -np 4 flow Gassnova_simulation_model_FF_SMEAHEIA_21.DATA --output-dir=CO2-Smeaheia-case
    ```
- Check the plots using `ResInsight`, 
  - `SGAS` variable animation. 


### Scripting the post processing steps

- Post-processing of the output files can be scripted with the [python API of ResInsight](https://api.resinsight.org/en/main/)
  -  Using the python library [`rips`](https://pypi.org/project/rips/)
- Sample example script is in `./post-processing-example.ipynb`
  - Set up the conda environment and dependences (before running the post processing script) as follows
    ```
        conda create -n co2 python=3.8
        conda activate co2
        pip install -r requirements.txt
    ```

### Sensitivity analysis

- 